from django.db import models


class Reunion(models.Model):
	titulo = models.CharField('Tema principal de reunion', max_length=40, help_text='Ingrese una breve descripcion')
	fecha = models.DateField('Fecha en la que se realizara la reunion')
	hora_inicio = models.TimeField('Hora de inicio')
	hora_fin = models.TimeField('Hora de finalización')
	asunto = models.TextField('Tema detallado a tratar en la reunion', max_length=1200, help_text='Ingrese una descripcion detallada')

	def __str__(self):
		return '%s' % (self.titulo)

	class Meta:
		verbose_name_plural = 'reuniones'
