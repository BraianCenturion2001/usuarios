from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse_lazy
from django.views.generic import TemplateView, ListView, DetailView
from django.views.generic.edit import UpdateView, CreateView, DeleteView
from .models import Reunion
from .forms import ReunionForm



class ReunionCreate(CreateView):
	model = Reunion
	form_class = ReunionForm
	template_name = 'reunion/reunion_form.html'
	success_url = reverse_lazy('reunion')

class ReunionView(ListView):
	model = Reunion
	template_name = 'reunion/reunion_list.html'
	context_object_name = 'reuniones'

class ReunionDetail(DetailView):
	model = Reunion
	template_name = 'reunion/reunion_detail.html'

	def get_object(self):
		id_ = self.kwargs.get("id")
		return get_object_or_404(Reunion, id=id_)

class ReunionDelete(DeleteView):
    model = Reunion
    template_name = 'reunion/reunion _delete.html'
    success_url = reverse_lazy('reunion:reunion_list')
