from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse_lazy
from django.contrib.auth import login, authenticate
from django.views.generic import TemplateView, DetailView
from django.views.generic.list import ListView
from django.views.generic.edit import UpdateView, CreateView, DeleteView
from .models import Perfil, Noticia
from .forms import RegistrarForm, EditPerfilForm, NoticiaForm
from django.contrib.auth.views import LoginView, LogoutView

class RegistrarView(CreateView):
	model = Perfil
	form_class = RegistrarForm

	def form_valid(self, form):
		form.save()
		usuario = form.cleaned_data.get('username')
		password = form.cleaned_data.get('password1')
		usuario = authenticate(username=usuario, password=password)
		login(self.request, usuario)
		return redirect('/')

class BienvenidaView(TemplateView):
	template_name = 'perfiles/bienvenida.html'

class CerrarSesionView(LogoutView):
	pass

class IniciarSesionView(LoginView):
	template_name = 'perfiles/iniciar_sesion.html'

class PerfilUpdateView(UpdateView):
	form_class = 'EditPerfilForm'
	template_name = 'perfiles/perfil_edit.html'
	queryset = Perfil.objects.all()

	def get_object(request):
		id_ = Perfil.objects.get('dni')
		return get_object_or_404(Perfil, id=id_)

	def form_valid(self, form):
		print(form.cleaned_data)
		return super().form_valid(form)

class NoticiaCreate(CreateView):
	model = Noticia
	template_name = 'noticias/noticia_form.html'
	form_class = NoticiaForm
	success_url = reverse_lazy('noticia_list')

class NoticiaView(ListView):
	model = Noticia
	template_name = 'noticias/noticia_list.html'
	context_object_name = 'noticias'


class NoticiaDetalle(DetailView):
	model = Noticia
	template_name = 'noticias/noticia_detail.html'

	def get_object(self):
		id_ = self.kwargs.get("id")
		return get_object_or_404(Noticia, id=id_)

class NoticiaDelete(DeleteView):
    model = Noticia
    template_name = 'noticias/noticia_delete.html'
    success_url = reverse_lazy('perfiles:noticia_list')

def noticia_delete(request, id_noticia):
	noticia = Noticia.objects.get(id=id_noticia)
	if request.method == 'POST':
		mascota.delete()
		return redirect('perfiles:list_noticia')
	return render(request, 'perfiles/noticia_detalle.html', {'noticia': noticia_detalle})
