from django.db import models
from django.contrib.auth.models import User


class Perfil(models.Model):
	usuario = models.OneToOneField(User, on_delete=models.CASCADE)
	ano_cursado = models.IntegerField(default=1)
	dni = models.IntegerField(default=0, primary_key=True)

	def __str__(self): 
		return self.usuario.username

	class Meta():
		verbose_name_plural = 'perfiles'

class Noticia(models.Model):
	titulo = models.CharField('Ingrese el Título', max_length=60)
	fecha_pub = models.DateField('Ingrese Fecha de Publicacion')
	breve = models.CharField('Ingrese una BREVE descripción sobre la nota', max_length=200)
	contenido = models.TextField('Detalle la Noticia', max_length=600)
	foto = models.ImageField('Foto sobre la Noticia', upload_to='noticia', help_text='Suba una Foto')

	def __str__(self):
		return '%s, %s' % (self.titulo, self.fecha_pub)

	class Meta():
		ordering = ['fecha_pub']
		verbose_name_plural = 'noticias'
