from django import forms
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User
from .models import Perfil, Noticia

class RegistrarForm(UserCreationForm):
    año_cursado = forms.IntegerField(required=True, help_text='Ingrese el año en el que cursa')
    dni = forms.CharField(required=True, help_text='Ingrese número de documento')

    class Meta:
        model = User
        fields = (
            'username',
            'email',
            'first_name',
            'last_name',
            'password1',
            'password2',
        )

class EditPerfilForm(forms.ModelForm):
    año_cursado = forms.IntegerField(required=True, help_text='Ingrese el año en el que cursa')

    class Meta:
        model = User
        fields = ('username', 'email', 'first_name', 'last_name')

class NoticiaForm(forms.ModelForm):
    class Meta():
        model = Noticia
        fields = ('titulo', 'fecha_pub', 'breve', 'contenido', 'foto')
