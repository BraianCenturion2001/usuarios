# Generated by Django 2.2.6 on 2019-11-11 17:47

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Noticia',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('titulo', models.CharField(max_length=60, verbose_name='Ingrese el Título')),
                ('fecha_pub', models.DateField(verbose_name='Ingrese Fecha de Publicacion')),
                ('breve', models.CharField(max_length=200, verbose_name='Ingrese una BREVE descripción sobre la nota')),
                ('contenido', models.TextField(max_length=600, verbose_name='Detalle la Noticia')),
                ('foto', models.ImageField(help_text='Suba una Foto', upload_to='noticia', verbose_name='Foto sobre la Noticia')),
            ],
            options={
                'verbose_name_plural': 'noticias',
                'ordering': ['fecha_pub'],
            },
        ),
        migrations.CreateModel(
            name='Perfil',
            fields=[
                ('ano_cursado', models.IntegerField(default=1)),
                ('dni', models.IntegerField(default=0, primary_key=True, serialize=False)),
                ('usuario', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name_plural': 'perfiles',
            },
        ),
    ]
