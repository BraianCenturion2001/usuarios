from django.contrib import admin
from .models import Perfil, Noticia

# Register your models here.

@admin.register(Perfil)
class PerfilAdmin(admin.ModelAdmin):
	list_display = ('usuario', 'ano_cursado', 'dni')

@admin.register(Noticia)
class NoticiaAdmin(admin.ModelAdmin):
	list_display = ('titulo', 'fecha_pub', 'breve', 'contenido', 'foto')
