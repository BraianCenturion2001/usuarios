from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings
from perfiles.views import (
	BienvenidaView, RegistrarView, 
	IniciarSesionView, CerrarSesionView, 
	PerfilUpdateView, NoticiaCreate, 
	NoticiaDetalle, NoticiaDelete, 
	noticia_delete, NoticiaView,
	 )
from reunion.views import ReunionView, ReunionCreate, ReunionDetail, ReunionDelete

urlpatterns = [
	path('admin/', admin.site.urls),
	path('', BienvenidaView.as_view(), name='bienvenida'),
	path('reunion/', ReunionView.as_view(), name='reunion'),
	path('reunion/crear/', ReunionCreate.as_view(), name='reunion_crear'),
	path('reunion/detalle/<int:pk>', ReunionDetail.as_view(), name='reunion_detail'),
	path('reunion/borrar/<int:pk>', ReunionDelete.as_view(), name='reunion_eliminar'),
	path('registrate/', RegistrarView.as_view(), name='sign_up'),
	path('inicia-sesion/', IniciarSesionView.as_view(), name='sign_in'),
	path('cerrar-sesion/', CerrarSesionView.as_view(), name='sign_out'),
	path('editar/<int:id>', PerfilUpdateView.as_view(), name='editar_usuario'),
	path('noticias/', NoticiaView.as_view(), name='noticia_list'),
	path('noticias/crear', NoticiaCreate.as_view(), name='noticia_crear'),
	path('noticias/detalle/<int:pk>', NoticiaDetalle.as_view(), name='noticia_detalle'),
	path('noticias/eliminar/<int:pk>', NoticiaDelete.as_view(), name='noticia_eliminar'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
